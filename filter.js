const filter = (elements, cb) => {
    let filteredArray = [];

    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements) === true) {
                filteredArray.push(elements[index]);
            }
        }
    }

    return filteredArray;
}

module.exports = filter;