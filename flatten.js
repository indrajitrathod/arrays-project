const filter=require('./filter');
const reduce = require('./reduce');
const flatten = (elements, depth = 1) => {

    if (!Array.isArray(elements)) {
        return elements;
    }

    return filter(depth > 0 ?
        reduce(elements, (acc, val) => acc.concat(flatten(val, depth - 1)), [])
        : elements.slice(), (element)=>{
            return element!==undefined;
        })
}

module.exports = flatten;
