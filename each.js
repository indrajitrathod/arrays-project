const each = (elements, cb) => {

    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        for (let index = 0; index < elements.length; index++) {
            cb(elements[index], index);
        }
    }

}

module.exports = each;