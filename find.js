const find = (elements, cb) => {

    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index])) {
                return elements[index];
            }
        }
    }

    return undefined;
}

module.exports = find;
