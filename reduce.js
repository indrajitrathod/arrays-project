const reduce = (elements, cb, startingValue) => {
    let index;
    let initialValue;

    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        [initialValue, index] = startingValue ? [startingValue, 0] : elements[0] ? [elements[0], 1] : [[], 0];

        while (index < elements.length) {
            initialValue = cb(initialValue, elements[index], index, elements);
            index++;
        }
    }

    return initialValue;
}

module.exports = reduce;