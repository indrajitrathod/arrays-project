const assert = require('chai').assert;
const { describe } = require('mocha');

const each = require('../each');
const items = [1, 2, 3, 4, 5, 5];


describe('each()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = each();
    });

    it('Function shouldn\'t throw error when first argument is not an array', () => {
        each({}, (item, index) => {
            console.log(item, index);
        });

        each(false, (item, index) => {
            console.log(item, index);
        });

        each(5, (item, index) => {
            console.log(item, index);
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        each(items, [12345, 3]);
        each(items, {});
        each(items, true);
        each(items, 564.67);
        each(items, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        each(items, ({ }, index) => {
            console.log(index);
        });
    });

    it('Function should give expected result', () => {
        each(items, (item, index) => {
            console.log(item, index);
        });
    });

});
