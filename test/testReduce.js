const assert = require('chai').assert;
const { describe } = require('mocha');

const reduce = require('../reduce');
const items = [1, 2, 3, 4, 5, 5];


describe('reduce()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = reduce();
    });

    it('Function shouldn\'t throw error when first argument is not an array', () => {
        reduce({}, (item, index) => {
            console.log(item, index);
        });

        reduce(false, (item, index) => {
            console.log(item, index);
        });

        reduce(5, (item, index) => {
            console.log(item, index);
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        reduce(items, [12345, 3]);
        reduce(items, {});
        reduce(items, true);
        reduce(items, 564.67);
        reduce(items, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        reduce(items, ({ }, index) => {
            return index;
        });
    });

    it('Function should give expected result', () => {
        const result = reduce(items, (previous, current) => {
            return previous + current;

        }, 0);

        const result2 = reduce(items, (previous, current) => {
            return previous + current;

        }, 10);

        assert.equal(result, 20);
        assert.equal(result2, 30);
    });

});
