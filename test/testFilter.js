const assert = require('chai').assert;
const { describe } = require('mocha');

const filter = require('../filter');
const items = [1, 2, 3, 4, 5, 5];

describe('filter()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = filter();
    });

    it('Function shouldn\'t throw error when first argument is not an array', () => {
        filter({}, items, (item) => {
            if (item > 3) {
                return true;
            }
        });

        filter(false, items, (item) => {
            if (item > 3) {
                return true;
            }
        });

        filter(5, items, (item) => {
            if (item > 3) {
                return true;
            }
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        filter(items, [12345, 3]);
        filter(items, {});
        filter(items, true);
        filter(items, 564.67);
        filter(items, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        filter(items, ({ }, index) => {
            console.log(index);
        });
    });

    it('Function should give expected result', () => {
        const result = filter(items, (item) => {
            if (item > 3) {
                return true;
            }
        });

        const result2 = filter(items, (item) => {
            if (item < 0) {
                return true;
            }
        });

        assert.deepEqual(result, [4, 5, 5]);
        assert.deepEqual(result2, []);
    });

    it('Function should return empty array when input is not array type', () => {
        const result = filter({ name: 'indrajit' }, (item) => {
            if (item > 3) {
                return true;
            }
        });

        assert.deepEqual(result, []);
    });

});
