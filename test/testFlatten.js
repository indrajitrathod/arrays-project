const assert = require('chai').assert;
const { describe } = require('mocha');

const flatten = require('../flatten');
const nestedArray = [1, [2], [[3]], [[[4]]]];

describe('flatten()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = flatten();
    });

    it('Function shouldn\'t throw error when first argument is not an array and second argument is an callback function', () => {
        flatten({}, (item, index) => {
            console.log(item, index);
        });

        flatten(false, (item, index) => {
            console.log(item, index);
        });

        flatten(5, (item, index) => {
            console.log(item, index);
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        flatten(nestedArray, [12345, 3]);
        flatten(nestedArray, {});
        flatten(nestedArray, true);
        flatten(nestedArray, 564.67);
        flatten(nestedArray, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        flatten(nestedArray, ( {}, index) => {
            console.log(index);
        });
    });

    it('Function should give expected result', () => {
        const result = flatten(nestedArray);
        const result2 = flatten([1, 2, , , 3, 'hello', [5, 6]]);

        assert.deepEqual(result, [1, 2, 3, 4]);
        assert.deepEqual(result2, [1, 2, 3, 'hello', 5, 6]);
    });

    it('Function should return empty array when inputs is not array type', () => {
        const result = flatten({ name: 'indrajit' }, (item) => {
            if (item > 3) {
                return true;
            }
        });

        assert.deepEqual(result, []);
    });

});
