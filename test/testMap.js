const assert = require('chai').assert;
const { describe } = require('mocha');

const map = require('../map');
const items = [1, 2, 3, 4, 5, 5];


describe('map()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = map();
    });

    it('Function shouldn\'t throw error when first argument is not an array', () => {
        map({}, (item, index) => {
            console.log(item, index);
        });

        map(false, (item, index) => {
            console.log(item, index);
        });

        map(5, (item, index) => {
            console.log(item, index);
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        map(items, [12345, 3]);
        map(items, {});
        map(items, true);
        map(items, 564.67);
        map(items, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        map(items, ({ }, index) => {
            console.log(index);
        });
    });

    it('Function should give expected result', () => {
        const result = map(items, (item) => {
            return item * item;
        });

        assert.deepEqual(result, [1, 4, 9, 16, 25, 25]);
    });

    it('Function should return empty array when input is not array type', () => {
        const result = map({ name: 'indrajit' }, (item) => {
            return item * item;
        });

        assert.deepEqual(result, []);
    });

});
