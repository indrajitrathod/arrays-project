const assert = require('chai').assert;
const { describe } = require('mocha');

const find = require('../find');
const items = [1, 2, 3, 4, 5, 5];


describe('find()', () => {
    it('Function shouldn\'t throw error when no Arguments are passed', () => {
        const result = find();
    });

    it('Function shouldn\'t throw error when first argument is not an array', () => {
        find({}, (item) => {
            if (item > 3) {
                return true;
            }
        });

        find(false, (item) => {
            if (item > 3) {
                return true;
            }
        });

        find(5, (item) => {
            if (item > 3) {
                return true;
            }
        });

    });

    it('Function shouldn\'t throw error when second argument is not an callback function', () => {

        find(items, [12345, 3]);
        find(items, {});
        find(items, true);
        find(items, 564.67);
        find(items, { name: 'Indrajit' });

    });

    it('Function shouldn\'t throw error when arguments for callback functions is not of correct types', () => {
        find(items, ({ }, index) => {
            console.log(index);
        });
    });

    it('Function should give expected result', () => {
        const result = find(items, (item) => {
            if (item > 3) {
                return true;
            }
        });

        assert.equal(result, 4);
    });

    it('Function should return undefined when no match found', () => {
        const result = find(items, (item) => {
            if (item < 0) {
                return true;
            }
        });

        assert.equal(result, undefined);
    });

    it('Function should return undefined when input is not array type', () => {
        const result = find({ name: 'indrajit' }, (item) => {
            if (item > 3) {
                return true;
            }
        });

        assert.equal(result, undefined);
    });

});
