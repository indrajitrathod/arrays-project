const map = (elements, cb) => {
    const newArray = [];

    if (elements && cb && Array.isArray(elements) && typeof cb === 'function') {
        for (let index = 0; index < elements.length; index++) {
            newArray.push(cb(elements[index], index, elements));
        }
    }

    return newArray;
}

module.exports = map;